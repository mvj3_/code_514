    #import <CoreTelephony/CTCallCenter.h>  
  #import <CoreTelephony/CTCall.h>  
-(void)detectCall  
  {  
       CTCallCenter *callCenter = [[CTCallCenter alloc] init];  
      callCenter.callEventHandler=^(CTCall* call)  
      {  
          if (call.callState == CTCallStateDisconnected)  
          {  
              NSLog(@"Call has been disconnected");  
            
          }  
          else if (call.callState == CTCallStateConnected)  
          {  
              NSLog(@"Call has just been connected");  
          }  
             
          else if(call.callState == CTCallStateIncoming)  
          {  
              NSLog(@"Call is incoming");  
          
          }  
             
          else if (call.callState ==CTCallStateDialing)  
          {  
              NSLog(@"call is dialing");  
          }  
          else 
          {  
              NSLog(@"Nothing is done");  
          }  
      };  
  }